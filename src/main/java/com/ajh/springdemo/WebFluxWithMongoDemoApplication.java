package com.ajh.springdemo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WebFluxWithMongoDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebFluxWithMongoDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner dataLoader(UserReactiveRepository userRepo) {
		return new CommandLineRunner() {

			@Override
			public void run(String... args) throws Exception {
				userRepo.save(new User("andy", "andy.h.jiang@gmail.com")).subscribe();
				userRepo.save(new User("hjiang", "hjiang@163.com")).subscribe();
			}
		};
	}
}

